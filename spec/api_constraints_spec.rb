require 'rails_helper'

describe ApiConstraints do
  let(:api_constrait_v1) { ApiConstraints.new(version: 1)}
  let(:api_constrait_v2) { ApiConstraints.new(version: 2, default: true)}

  describe "matches?" do
    it "returns true when the version matches the 'Accept' header" do
      request = double(
        host: 'localhost:3000',
        headers: {:accept => 'application/vnd.glitchmusic.v1+json'})
      expect(api_constrait_v1.matches?(request)).to be true
    end

    it "returns the default version when 'default' option is specified" do
      request = double(host: 'localhost:3000', headers: {})
      expect(api_constrait_v2.matches?(request)).to be true
    end
  end
end