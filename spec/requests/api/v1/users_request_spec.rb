require 'rails_helper'

RSpec.describe "Api::V1::Users", type: :request do
  before do 
    headers = {
      "Accept" => "application/vnd.glitchmusic.v1, application/json",
      "Content-Type" => "application/json" 
    }
  end

  describe "GET #show" do
    before do
      @user = FactoryBot.create :user
      get api_user_path(id: @user.id), headers: headers
    end
    it "returns the infromation about a user in a hash" do
      expect(json[:email]).to eql @user.email
    end
    it "response with HTTP OK" do
      expect(response).to have_http_status(200)
    end
  end

  describe "POST #create" do
    context "when successfully created" do
      before do
        @user_attrs = FactoryBot.attributes_for :user
        post api_users_path, params: { user: @user_attrs }, headers: headers
      end
      it "returns the infromation about a user that has been created" do
        expect(json[:user][:email]).to eql @user_attrs[:email]
      end
      it "responds with HTTP status CREATED" do
        expect(response).to have_http_status(201)
      end
    end

    context "when is not create" do
      before do
        @user_attrs = {password: '1234567'}
        post api_users_path, params: { user: @user_attrs }, headers: headers
      end
      it "returns errors attribute in json" do
        expect(json).to have_key(:errors)
      end
      it "returns json errors why the user could not created" do
        expect(json[:errors][:email]).to include "can't be blank"
      end
      it "responds with an HTTP status UNPROCESSABLE ENTITY" do
        expect(response).to have_http_status(422)
      end
    end
  end

  describe "PUT/PATCH #update" do 
    context "when user is successfully updated" do
      before do 
        @user = FactoryBot.create :user
        patch api_user_path(id: @user.id), 
          params: { user: {email: "newmail@example.com"} }, 
          headers: headers
      end
      it "returns updated user json" do
        expect(json[:email]).to eql "newmail@example.com"
      end
      it "responds with HTTP status OK" do
        expect(response).to have_http_status(200)
      end
    end
  end

  describe "DELETE #destroy" do
    before do
      @user = FactoryBot.create :user
      delete api_user_path(@user)
    end
    it "responds with a HTTP status NO_CONTENT" do
      expect(response).to have_http_status(204)
    end
  end
end
