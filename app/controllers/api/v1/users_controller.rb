class Api::V1::UsersController < ApiController
  skip_before_action :authorize_request, only: [:create, :update, :show, :destroy]

  def show 
    json_response(User.find(params[:id]))
  end

  def create
    user = User.new(user_params)
    if user.save
      auth_token = AuthenticateUser.new(user.email, user.password).call
      response = { user: user, message: Message.account_created, auth_token: auth_token }
      json_response(response, :created)
    else
      json_response({ errors: user.errors }, :unprocessable_entity)
    end
  end

  def update
    user = User.find(params[:id])
    if user.update(user_params)
      json_response(user, :ok)
    else
      json_response({ errors: user.errors }, :unprocessable_entity)
    end
  end

  def destroy
    User.find(params[:id]).destroy
    head 204
  end

  private
    def user_params
      params.require(:user).permit(:email, :password, :password_confirmation)
    end
end
