require 'api_constraints'

Rails.application.routes.draw do
  root 'pages#index'
  devise_for :users
  namespace :api, defaults: { format: :json } do
    scope module: :v1, constraints: ApiConstraints.new(version: 1, default: true) do
      post 'auth/login', to: 'auth#authenticate'
      resources :users, :only => [:show, :create, :update, :destroy]
    end
  end
end
